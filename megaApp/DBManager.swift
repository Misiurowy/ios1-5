//
//  DBManager.swift
//  megaApp
//
//  Created by Użytkownik Gość on 29.01.2018.
//  Copyright © 2018 Użytkownik Gość. All rights reserved.
//

import Foundation

class DBMAnager{
    static var DbFileNAme: String = "sqlDatabase";
    static var DbDirectoryPath: String = "";
    static var dbConnection: OpaquePointer?;
    private var createSensorTableString:String = "CREATE TABLE sensors (name varchar(50) primary key, description varchar(200));"
    private var createReadingsTableString: String = "CREATE TABLE readings (tstamp timestamp, id serial primary key, sensor varchar(50) references sensors(name), value float(10,3));"
    func openDatabase(){
        var db: OpaquePointer? = nil
        getDatabasePath();
        print(type(of: self).DbDirectoryPath);
        if sqlite3_open(type(of: self).DbDirectoryPath, &db) == SQLITE_OK {
            print("Succesfully open conection to database");
            type(of: self).dbConnection = db;
        }else{
            print("Unable to open database.");
            return;
        }
        
        if(!dbExist()){
            createTables();
            createSensors();
        }
    }
    
    func initiateDatabase(){
        
    }
    
    func experimentMinMax()->String{
        let start = Date()
        let minSQL = "SELECT tstamp FROM readings order by tstamp ASC LIMIT 1;";
        let maxSQL = "SELECT tstamp FROM readings order by tstamp DESC LIMIT 1";
        var queryStatement:OpaquePointer? = nil;
        var result = "";
        if sqlite3_prepare_v2(type(of: self).dbConnection, minSQL, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                result += "Minimal timestamp: " + String(cString: sqlite3_column_text(queryStatement,0));
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
        
        if sqlite3_prepare_v2(type(of: self).dbConnection, maxSQL, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                result += "\nMaximal timestamp: " + String(cString: sqlite3_column_text(queryStatement,0));
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        result+="\nTime elapsed: \(measuredTime)";
        return result
    }
    func experimentAvrg()->String{
        let start = Date()
        let avrgSQL = "SELECT AVG(value) FROM readings;";
        var queryStatement:OpaquePointer? = nil;
        var result = "";
        if sqlite3_prepare_v2(type(of: self).dbConnection, avrgSQL, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                result += "Average reading: : " + String(cString: sqlite3_column_text(queryStatement,0));
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        result+="\nTime elapsed: \(measuredTime)";
        return result
    }
    func experimentAvrgGroupBy()->String{
        let start = Date()
        let avrgSQL = "SELECT AVG(value),sensor FROM readings GROUP BY sensor";
        var queryStatement:OpaquePointer? = nil;
        var result = "";
        if sqlite3_prepare_v2(type(of: self).dbConnection, avrgSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let value = String(cString: sqlite3_column_text(queryStatement,0));
                let index = value.index(value.startIndex, offsetBy: 8)
                result += "Average reading for sensor "+String(cString: sqlite3_column_text(queryStatement,1))+": "+value.substring(to: index)+"\n"
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        result+="\nTime elapsed: \(measuredTime)";
        return result
    }
    func experimentGenerate(){
        
    }
    
    func deleteReadings(){
        let deleteSQL = "DELETE FROM readings";
        var queryStatement:OpaquePointer? = nil;
        if sqlite3_prepare_v2(type(of: self).dbConnection, deleteSQL, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_DONE {
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
    }
    
    private func dbExist()->Bool{
        let selectSQL = "SELECT * FROM sensors;";
        var queryStatement:OpaquePointer? = nil;
        var result:Bool = false;
        if sqlite3_prepare_v2(type(of: self).dbConnection, selectSQL, -1, &queryStatement, nil) == SQLITE_OK {
            if sqlite3_step(queryStatement) == SQLITE_ROW {
                result = true;
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
        }
        sqlite3_finalize(queryStatement);
        print("EXISTS: \(result)");
        return result;
    }
    
    private func getDatabasePath(){
        let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        type(of: self).DbDirectoryPath = docDir + "/" + type(of: self).DbFileNAme;
    }
    
    func gerReadings()-> [(String,String)]{
        var result: [(String,String)] = [];
        let selectSQL = "SELECT tstamp,value,sensor FROM readings";
        var queryStatement:OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(type(of: self).dbConnection, selectSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let tstamp = String(cString: sqlite3_column_text(queryStatement,0));
                let value = String(cString: sqlite3_column_text(queryStatement,1));
                let sensor = String(cString: sqlite3_column_text(queryStatement,2));
                result.append((tstamp, "reading: \(value), sensor: \(sensor)"));
            }
        }
        sqlite3_finalize(queryStatement);
        return result;
    }
    func getSensors()-> [(String,String)]{
        var result: [(String,String)] = [];
        let selectSQL = "SELECT name,description FROM sensors order by name ASC;";
        var queryStatement:OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(type(of: self).dbConnection, selectSQL, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let name = String(cString: sqlite3_column_text(queryStatement,0));
                let description = String(cString: sqlite3_column_text(queryStatement,1));
                print(description);
                result.append((name,description));
            }
            print("no more rows");
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            
        }else{
            print("couldnt prepare statement");
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            

        }
        sqlite3_finalize(queryStatement);
        return result;
    }
    
    
    func insertReadings(amount: Int)->String{
        let start = Date();
        var insertStatementString: String = "INSERT INTO readings(tstamp, sensor, value) VALUES ";
        for i in 0..<amount{
            let timestamp = NSDate().timeIntervalSince1970 - TimeInterval(arc4random_uniform(31556926));
            let dateFormatter = DateFormatter();
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss";
            let sensorNr: Int = Int(arc4random_uniform(20));
            var sensorName = "S";
            if(sensorNr>9){
                sensorName+=String(sensorNr);
            }else{
                sensorName+="0"+String(sensorNr);
            }
            let readingValue = (Float(arc4random()) / Float(UINT32_MAX))*100;
            insertStatementString += "('"+dateFormatter.string(from: Date(timeIntervalSince1970: timestamp))+"','"+sensorName+"',"+String(readingValue)+")";
            if(i<amount-1){
                insertStatementString+=",";
            }
        }
        insertStatementString+=";";
        var insertStatement: OpaquePointer? = nil;
        print(insertStatementString);
        if (sqlite3_prepare_v2(type(of: self).dbConnection, insertStatementString, -1, &insertStatement,nil)==SQLITE_OK) {
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("added readings");
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
                print("Couldn't add readings");
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            print("Couldn't prepare statement");
        }
        sqlite3_finalize(insertStatement);
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        let result="\nTime elapsed: \(measuredTime)";
        return result
    }
    
    private func createTables(){
        var createTableStatement: OpaquePointer? = nil;
        
        if sqlite3_prepare_v2(type(of: self).dbConnection, createSensorTableString, -1, &createTableStatement,nil)==SQLITE_OK {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Table sensors created");
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
                print("Couldn't create table sensor")
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            print("Couldn't prepare statement")
        }
        sqlite3_finalize(createTableStatement);
        
        if (sqlite3_prepare_v2(type(of: self).dbConnection, createReadingsTableString, -1, &createTableStatement,nil)==SQLITE_OK){
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Table sensors created");
            }else{
                print("Couldn't create table sensor")
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            print("Couldn't prepare statement")
        }
        
        sqlite3_finalize(createTableStatement);
    }
    
    private func createSensors(){
        var createSensorsString = "INSERT INTO sensors(name,description) VALUES";
        for i in 0...9{
            createSensorsString += "('S0"+String(i)+"','sensor numer \(i)'),";
        }
        for i in 10...19{
            createSensorsString += "('S"+String(i)+"','sensor numer \(i)')";
            if(i<19){
                createSensorsString+=",";
            }
        }
        createSensorsString+=";"
        
        var createTableStatement: OpaquePointer? = nil;
        
        print(createSensorsString);
        
        if (sqlite3_prepare_v2(type(of: self).dbConnection, createSensorsString, -1, &createTableStatement,nil)==SQLITE_OK) {
            if sqlite3_step(createTableStatement) == SQLITE_DONE {
                print("Sensors added");
            }else{
                print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
                print("Couldn't add sensors")
            }
        }else{
            print(String.init(cString: sqlite3_errmsg(type(of: self).dbConnection)));
            print("Couldn't prepare statement")
        }
        
        sqlite3_finalize(createTableStatement);
    }
}
