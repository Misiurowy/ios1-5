//
//  CoreDataManager.swift
//  megaApp
//
//  Created by Tomek on 03/02/2018.
//  Copyright © 2018 Użytkownik Gość. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataManager{
    private static func addSensors(){
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            print("Cannot get appDeelegate in addSensors")
            return
        }
        print("fetched appDeelegate in addSensors")
        let moc = ad.persistentContainer.viewContext
        for i in 0..<20{
            var name = "";
            if(i<10){
                name = "S0\(i)"
            }else{
                name = "S\(i)"
            }
            let entity = NSEntityDescription.entity  (forEntityName: "Sensor", in: moc)
            let sensor = NSManagedObject(entity: entity!, insertInto: moc)
            sensor.setValue(name, forKey: "name")
            sensor.setValue("Sensor numer "+name, forKey: "desc")
            try? moc.save()
        }
    }
    
    static func addReadings(amount: Int)->String{
        let start = Date()
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            return "";
        }
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSManagedObject>(entityName: "Sensor")
        var sensors = try? moc.fetch(fr) as! [Sensor]
        for _ in 0..<amount{
            let sensorNr = Int(arc4random_uniform(20));
            let entity = NSEntityDescription.entity  (forEntityName: "Readings", in: moc)
            let reading = NSManagedObject(entity: entity!, insertInto: moc) as! Readings
            reading.from = sensors![sensorNr]
            reading.tstamp = NSDate().addingTimeInterval(-TimeInterval(arc4random_uniform(31556926)))
            reading.value = (Double(arc4random()) / Double(UINT32_MAX))*100;
        }
        try? moc.save()
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        let result="\nTime elapsed: \(measuredTime)";
        return result
    }
    
    static func experimentMinMax()->String{
        let start = Date()
        var result = "";
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            return "";
        }
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSManagedObject>(entityName: "Readings")
        fr.fetchLimit = 1;
        fr.sortDescriptors = [NSSortDescriptor(key: "tstamp", ascending: true)]
        let min = try? moc.fetch(fr).first as! Readings
        let maxfr = NSFetchRequest<NSManagedObject>(entityName: "Readings")
        maxfr.fetchLimit = 1
        maxfr.sortDescriptors = [NSSortDescriptor(key: "tstamp", ascending: false)]
        let max = try? moc.fetch(maxfr).first as! Readings
        result+="Max value: \(max!.tstamp!)\nMin value: \(min!.tstamp!)"
        let end = NSDate()
        let measuredTime = end.timeIntervalSince(start);
        result+="\nTime elapsed: \(measuredTime)";
        return result
    }
    
    static func getReadings()->[(String,String)]{
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            return []
        }
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"

        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSManagedObject>(entityName: "Readings")
        let readings = try? moc.fetch(fr) as! [Readings]
        var result: [(String,String)] = []
        for item in readings!{
            result.append((formatter.string(from: item.tstamp! as Date),"reading: \(item.value), sensor: "+item.from!.name!))
        }
        return result;
    }
    
    static func initDB(){
        if(!sensorsExist()){
            addSensors()
        }
    }
    
    private static func sensorsExist()->Bool{
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            print("Cannot get appDeelegate in getSensors")
            return false
        }
        print("fetched get appDeelegate in getSensors")
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSManagedObject>(entityName: "Sensor")
        let sensors = try? moc.fetch(fr) as [NSManagedObject]
        if sensors!.count<1{
            return false;
        }else{
            return true;
        }
    }
    
    static func getSensors()->[(String,String)]{
        var result: [(String,String)] = []
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            print("Cannot get appDeelegate in getSensors")
            return []
        }
        print("fetched get appDeelegate in getSensors")
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSManagedObject>(entityName: "Sensor")
        let sensors = try? moc.fetch(fr) as [NSManagedObject]
        print("got \(sensors!.count) results")
        for sensor in sensors!{
            result.append(("\(sensor.value(forKey: "name")!)","\(sensor.value(forKey: "desc")!)"));
        }
        print(result);
        return result
    }
    

    
    static func deleteReadings(){
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            return
        }
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSFetchRequestResult>(entityName: "Readings")
        let batchDelete = NSBatchDeleteRequest(fetchRequest: fr)
        try? moc.execute(batchDelete)
    }
    
    static func deleteSensors(){
        guard let ad = UIApplication.shared.delegate  as? AppDelegate else {
            return
        }
        let moc = ad.persistentContainer.viewContext
        let fr = NSFetchRequest<NSFetchRequestResult>(entityName: "Sensor")
        let batchDelete = NSBatchDeleteRequest(fetchRequest: fr)
        try? moc.execute(batchDelete)
    }
    
    
}
