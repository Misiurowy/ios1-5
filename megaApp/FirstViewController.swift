//
//  FirstViewController.swift
//  megaApp
//
//  Created by Użytkownik Gość on 01.12.2017.
//  Copyright © 2017 Użytkownik Gość. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    var dbManager: DBMAnager = DBMAnager();
    
    @IBAction func Avrg(_ sender: Any) {
        let text = dbManager.experimentAvrg();
        TextView.text = text;
    }
    @IBAction func AvrgBySensor(_ sender: Any) {
        let text = dbManager.experimentAvrgGroupBy();
        TextView.text = text;
    }
    @IBAction func exeprimentTwo(_ sender: Any) {
        let text = CoreDataManager.experimentMinMax();
        TextView.text = text;
    }
    
    @IBAction func DeleteAction(_ sender: Any) {
        CoreDataManager.deleteReadings();
    }
    
    @IBAction func AddReadingsClicked(_ sender: Any) {
        
        if let tmp = textFieldInput.text{
            if let tmpInt = Int(tmp){
                let result = CoreDataManager.addReadings(amount: tmpInt);
                TextView.text="\n"+result;
            }
        }
    }
    
    @IBOutlet weak var textFieldInput: UITextField!
    @IBOutlet weak var TextView: UITextView!
    override func viewDidLoad(){
        super.viewDidLoad()
        DBMAnager().openDatabase();
        TextView.text = ""
        CoreDataManager.initDB()

        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

